package com.stratpoint.frontend.controllers;

import com.stratpoint.frontend.models.Name;
import com.stratpoint.frontend.models.Profile;
import com.stratpoint.frontend.models.ProfileDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/")
@Controller
public class WebController {

	private static final Logger LOG = LoggerFactory.getLogger(WebController.class);

	@GetMapping
	public String getHomePage(ArrayList<ProfileDTO> profileDTOList) {

		Client client = null;
		ArrayList<ProfileDTO> profileDTOS;
		try{
			client = ClientBuilder.newClient();

			profileDTOS = client
					.target("http://localhost:8080/profile")
					.request("application/json")
					.get(new GenericType<ArrayList<ProfileDTO>>(){});
		}
		finally{
			if(client != null){
				client.close();
			}
		}
		profileDTOList.addAll(profileDTOS);
		return "home";
	}

	@GetMapping("/{id}")
	public String getProfile(@PathVariable("id") String id,  Profile profile){
		Client client = null;
		Profile profileFromServer;
		try{
			client = ClientBuilder.newClient();

			profileFromServer = client
					.target("http://localhost:8080/profile/" + id)
					.request("application/json")
					.get(Profile.class);
		}
		finally{
			if(client != null){
				client.close();
			}
		}

		profile.setId(profileFromServer.getId());
		profile.setActive(profileFromServer.getActive());
		profile.setBlocked(profileFromServer.getBlocked());
		profile.setBalance(profileFromServer.getBalance());
		profile.setPicture(profileFromServer.getPicture());
		profile.setAge(profileFromServer.getAge());
		profile.setName(profileFromServer.getName());
		profile.setEmail(profileFromServer.getEmail());
		profile.setPhone(profileFromServer.getPhone());
		profile.setAddress(profileFromServer.getAddress());
		profile.setProfile(profileFromServer.getProfile());
		profile.setDateRegistered(profileFromServer.getDateRegistered());
		return "profile";
	}

	@GetMapping("/search")
	public String getHomePageSearch(@RequestParam("name")String name, ArrayList<ProfileDTO> profileDTOList) {

		Client client = null;
		ArrayList<ProfileDTO> profileDTOS;
		try{
			client = ClientBuilder.newClient();

			profileDTOS = client
					.target("http://localhost:8080/profile/search?name=" + name)
					.request("application/json")
					.get(new GenericType<ArrayList<ProfileDTO>>(){});
		}
		finally{
			if(client != null){
				client.close();
			}
		}
		profileDTOList.addAll(profileDTOS);
		return "home";
	}
}