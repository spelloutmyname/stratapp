<%--
  Created by IntelliJ IDEA.
  User: carl.adrian
  Date: 8/9/18
  Time: 6:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <c:url value="/css/profile.css" var="jstlCss" />
    <link href="${jstlCss}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
    <title>Profile</title>
</head>
<body>
    <div>
        <div class="empty-space">

        </div>
        <div class="container" id="profile-container">
            <div class="row" id="profile-header">
                <img src="${profile.picture}"/>
                <span id="profile-name">
                    ${profile.name.first} ${profile.name.middle} ${profile.name.last}
                </span>
            </div>
            <div id="profile-id" class="row">
                <div class="col-sm-1">
                    ID:
                </div>
                <div class="col-sm-3">
                    ${profile.id}
                </div>
            </div>
            <div id="profile-summary" class="row">
                <div class="col-sm-1">
                    Profile:
                </div>
                <div class="col-sm-11">
                   <p> ${profile.profile}</p>
                </div>
            </div>
            <div id="profile-email" class="row">
                <div class="col-sm-1">
                    Email:
                </div>
                <div class="col-sm-3">
                    ${profile.email}
                </div>
            </div>
            <div id="profile-phone" class="row">
                <div class="col-sm-1">
                    Phone:
                </div>
                <div class="col-sm-3">
                    ${profile.phone}
                </div>
            </div>
            <div id="profile-address" class="row">
                <div class="col-sm-1">
                    Address:
                </div>
                <div class="col-sm-3">
                    ${profile.address}
                </div>
            </div>
            <div id="profile-age" class="row">
                <div class="col-sm-1">
                    Age:
                </div>
                <div class="col-sm-3">
                    ${profile.age}
                </div>
            </div>
            <div id="profile-balance" class="row">
                <div class="col-sm-1">
                    Balance:
                </div>
                <div class="col-sm-3">
                    ${profile.balance}
                </div>
            </div>
        </div>
    </div>
</body>
</html>
