<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

	<title>Home</title>
	<!-- Access the bootstrap Css like this,
		Spring boot will handle the resource mapping automcatically -->
	<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

	<!--
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
	<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />
</head>
<body>

	<div class="container">

		<div class="starter-template">
			<h1>Home Page</h1>
			<h2>Profile List</h2>
		</div>

		<div>
			<form method = get action="http://localhost:8888/search" >
				<input type="text" name="name">
			</form>
		    <table id="profile-table" style="width:100%">
				<tr>
					<th>Name</th>
				  	<th align="center">Age</th>
				  	<th align="center">Active</th>
				  	<th align="center">Blocked</th>
				</tr>
				<c:forEach items="${profileDTOList}" var="profileDTO">
					<tr>
						<td><a href="/${profileDTO.id}"><c:out value="${profileDTO.name.first} "/><c:out value="${profileDTO.name.middle} "/><c:out value="${profileDTO.name.last}"/></a></td>
						<td align="center"><c:out value="${profileDTO.age}"/></td>
						<td align="center"><input type="checkbox" name="active" ${profileDTO.active ? 'checked' : ''}></td>
						<td align="center"><input type="checkbox" name="blocked" ${profileDTO.blocked ? 'checked' : ''}></td>
					</tr>
				</c:forEach>
            </table>
		</div>

	</div>

	<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>