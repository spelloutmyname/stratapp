package com.stratpoint.backend.utils;

import com.stratpoint.backend.models.Profile;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import java.util.List;

@Component
public class ApiUtils {

	public List<Profile> retrieveProfilesFromJsonAPI(){

		Client client = null;
		try{
			client = ClientBuilder.newClient();

			return client
					.target("http://s3-ap-southeast-1.amazonaws.com/fundo/js/profiles.json")
					.request("application/json")
					.get(new GenericType<List<Profile>>(){});
		}
		finally{
			if(client != null){
				client.close();
			}
		}
	}
}
