package com.stratpoint.backend.services;

import com.stratpoint.backend.models.Profile;
import com.stratpoint.backend.models.ProfileDTO;
import com.stratpoint.backend.utils.ApiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class ProfileService {

	@Autowired
	ApiUtils apiUtils;

	public List<ProfileDTO> getProfiles(){
		List<ProfileDTO> profileDTOList = new ArrayList<>();

		List<Profile> profiles = apiUtils.retrieveProfilesFromJsonAPI();
		profiles.sort(Comparator.comparing(Profile::getAge));
		profiles.stream().forEach(profile ->
				profileDTOList.add(
						new ProfileDTO(
								profile.getId(),
								profile.getName(),
								profile.getAge(),
								profile.getActive(),
								profile.getBlocked()
						)
				)
		);

		profileDTOList.sort(
				Comparator.comparing(profileDTO -> profileDTO.getName().getFirst())
		);
		profileDTOList.sort(
				Comparator.comparing(ProfileDTO::getAge)
		);
		profileDTOList.sort(
				Comparator.comparing(ProfileDTO::getActive)
		);
		return profileDTOList;
	}

	public Profile getProfile(String id){
		List<Profile> profiles = apiUtils.retrieveProfilesFromJsonAPI();

		Optional<Profile> optionalProfile = profiles.stream().filter(profile -> profile.getId().equals(id)).findFirst();

		if (optionalProfile.isPresent()){
			return optionalProfile.get();
		}else {
			throw new NotFoundException("Profile Not Found");
		}
	}

	public List<ProfileDTO> searchByName(String name){
		List<Profile> profiles = apiUtils.retrieveProfilesFromJsonAPI();

		Pattern pattern = Pattern.compile(name);

		List<Profile> filteredProfiles = profiles.stream().filter(profile ->
				profile.getName().getFirst().toLowerCase().contains(name.toLowerCase())
						|| profile.getName().getLast().toLowerCase().contains(name.toLowerCase())
		).collect(Collectors.toList());


		if (filteredProfiles.isEmpty()){
			throw new NotFoundException("Profile Not Found");
		}else {
			List<ProfileDTO> profileDTOList = new ArrayList<>();
			filteredProfiles.stream().forEach(profile ->
					profileDTOList.add(
							new ProfileDTO(
									profile.getId(),
									profile.getName(),
									profile.getAge(),
									profile.getActive(),
									profile.getBlocked()
							)
					)
			);
			return profileDTOList;
		}
	}
}
