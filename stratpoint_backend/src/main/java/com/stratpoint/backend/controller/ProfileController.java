package com.stratpoint.backend.controller;

import com.stratpoint.backend.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;

@Controller
@Path("profile")
@Produces(MediaType.APPLICATION_JSON)
public class ProfileController {

	@Autowired
	ProfileService profileService;

	@GET
	public Response getProfiles(){
		return Response.ok(profileService.getProfiles()).build();

	}

	@GET
	@Path("{profile-id}")
	public Response getProfile(@PathParam("profile-id") String id){
		try {
			return Response.ok(profileService.getProfile(id)).build();
		}catch (NotFoundException e){
			return Response.status(Response.Status.NOT_FOUND).build();
		}catch (Exception e){
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Path("/search")
	public Response searchByName(@QueryParam("name") String name){
		try {
			return Response.ok(profileService.searchByName(name)).build();
		}catch (NotFoundException e){
			return Response.ok(Arrays.asList()).build();
		}catch (Exception e){
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
