package com.stratpoint.backend.models;

public class ProfileDTO {

	private String id;
	private Name name;
	private int age;
	private boolean active;
	private boolean blocked;

	public ProfileDTO(String id, Name name, int age, boolean active, boolean blocked) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.active = active;
		this.blocked = blocked;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
}
