package com.stratpoint.backend.services;

import com.stratpoint.backend.models.Name;
import com.stratpoint.backend.models.Profile;
import com.stratpoint.backend.models.ProfileDTO;
import com.stratpoint.backend.utils.ApiUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;


@RunWith(MockitoJUnitRunner.class)
public class ProfileServiceTest {
	public static final String EXPECTED_ID = "third id";
	public static final String FAKE_ID = "fake id";
	public static final String PROFILE_NOT_FOUND = "Profile Not Found";
	public static final String FIRST_ID = "first id";
	public static final String SECOND_ID = "second id";
	public static final String THIRD_ID = EXPECTED_ID;
	public static final String EXPECTED_FIRST_NAME = "Firstname 1";
	public static final String FIRSTNAME_1 = EXPECTED_FIRST_NAME;
	public static final String FIRSTNAME_2 = "Firstname 2";
	public static final String FIRSTNAME_3 = "Firstname 3";
	public static final String FAKE_NAME = "fake name";
	public static final String EXPECTED_LAST_NAME = "Lastname 3";
	public static final String LASTNAME_1 = "Lastname 1";
	public static final String LASTNAME_2 = "Lastname 2";
	public static final String LASTNAME_3 =  EXPECTED_LAST_NAME;

	@InjectMocks
	ProfileService profileService;

	@Mock
	ApiUtils apiUtils;

	List<Profile> profiles = new ArrayList<>();

	@Before
	public void setup(){
		Profile profile = new Profile();
		profile.setName(new Name());
		profile.getName().setFirst(FIRSTNAME_1);
		profile.getName().setLast(LASTNAME_1);
		profile.setId(FIRST_ID);
		profiles.add(profile);

		profile = new Profile();
		profile.setName(new Name());
		profile.getName().setFirst(FIRSTNAME_2);
		profile.getName().setLast(LASTNAME_2);
		profile.setId(SECOND_ID);
		profiles.add(profile);

		profile = new Profile();
		profile.setName(new Name());
		profile.getName().setFirst(FIRSTNAME_3);
		profile.getName().setLast(LASTNAME_3);
		profile.setId(THIRD_ID);
		profiles.add(profile);

	}

	@Test
	public void getProfiles(){
		given(apiUtils.retrieveProfilesFromJsonAPI()).willReturn(profiles);
		List<ProfileDTO> profileDTOList = profileService.getProfiles();
		assertThat(profileDTOList.size()).isEqualTo(3);
	}

	@Test
	public void getProfile(){
		given(apiUtils.retrieveProfilesFromJsonAPI()).willReturn(profiles);
		Profile profile = profileService.getProfile(THIRD_ID);
		assertThat(profile.getId()).isEqualTo(EXPECTED_ID);
	}

	@Test
	public void getProfileNotFound(){
		given(apiUtils.retrieveProfilesFromJsonAPI()).willReturn(profiles);
		assertThatExceptionOfType(NotFoundException.class).isThrownBy(()->profileService.getProfile(FAKE_ID));
		assertThatThrownBy(()->profileService.getProfile(FAKE_ID)).hasMessage(PROFILE_NOT_FOUND);
	}

	@Test
	public void searchByFirstName(){
		given(apiUtils.retrieveProfilesFromJsonAPI()).willReturn(profiles);
		ProfileDTO profileDTO = profileService.searchByName(FIRSTNAME_1).get(0);
		assertThat(profileDTO.getName().getFirst()).isEqualTo(EXPECTED_FIRST_NAME);
	}

	@Test
	public void searchByLastName(){
		given(apiUtils.retrieveProfilesFromJsonAPI()).willReturn(profiles);
		ProfileDTO profileDTO = profileService.searchByName(LASTNAME_3).get(0);
		assertThat(profileDTO.getName().getLast()).isEqualTo(EXPECTED_LAST_NAME);
	}

	@Test
	public void searchByNameNotFound(){
		given(apiUtils.retrieveProfilesFromJsonAPI()).willReturn(profiles);
		assertThatExceptionOfType(NotFoundException.class).isThrownBy(()->profileService.searchByName(FAKE_NAME));
		assertThatThrownBy(()->profileService.getProfile(FAKE_ID)).hasMessage(PROFILE_NOT_FOUND);
	}
}